variable "location" {
  description = "Azure Region where the AKS module should be provisioned"
  default     = "Central US"
}

variable "resource_group_name" {
  description = "Azure Resource Group for AKS module"
  default     = "hashi-atyeti-terraform-aks-staging"
}

module "aks_winnodepool_module" {
   source = "github.com:kawsark/terraform-azure-aks-winnodepool-module"
#  source  = "app.terraform.io/GitlabCI-demos/aks-winnodepool-module/azure"
#  version = "1.1.7"

  # Input variables
  environment         = "staging"
  location            = var.location
  resource_group_name = var.resource_group_name
}

// Outputs
output "kube_config" {
  value     = module.aks_winnodepool_module.kube_config
  sensitive = true
}
